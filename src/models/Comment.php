<?php
namespace Victor\models;

class Comment
{
    public int $id;
    public string $name;
    public string $text;

    public function __construct(string $name, string $text = '', int $id = null)
    {
        if ($id) {
            $this->id = $id;
        }
        $this->name = $name;
        $this->text = $text;
    }
}
