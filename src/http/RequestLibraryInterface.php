<?php
namespace Victor\http;

interface RequestLibraryInterface
{
    public function getResponse();
    public function getHttpStatusCode();
    public function get($url, $data = []);
    public function post($url, $data = []);
    public function put($url, $data = []);

}