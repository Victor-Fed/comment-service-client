<?php
namespace Victor\clients;

use Victor\http\RequestLibrary;
use Victor\http\RequestLibraryInterface;
use Victor\models\Comment;

class Example
{
    private string $baseUrl = 'http://example.com';
    private RequestLibraryInterface $http;
    /**
     * Example constructor.
     * @param RequestLibraryInterface|null $http
     */
    public function __construct(RequestLibraryInterface $http = null)
    {
        if ($http === null) {
            $this->http = new RequestLibrary();
        } else {
            $this->http = $http;
        }
    }

    /**
     * @return Comment[]
     */
    public function getComment() : array
    {
        $this->http->get($this->baseUrl . '/comments');
        $comments = [];
        if ($this->http->getHttpStatusCode() === 200) {
            foreach ($this->http->getResponse() as $comment) {
                $comments[] = $this->parseResponse($comment);
            }
        }
        return $comments;
    }

    /**
     * @param $comment
     * @return array
     * @throws \Exception
     */
    public function addComment(Comment $comment) : Comment
    {
        $this->http->post($this->baseUrl . '/comments', (array) $comment);
        return $this->parseResponse($comment);
    }

    /**
     * @param int $id
     * @param Comment $comment
     * @return Comment
     */
    public function updateComment(int $id, Comment $comment) : Comment
    {
        $this->http->put($this->baseUrl . '/comments/' . $id, (array) $comment);
        return $this->parseResponse($comment);
    }

    public function getHttpStatusCode(): int
    {
        return $this->http->getHttpStatusCode();
    }

    protected function parseResponse($response) : Comment
    {
        $name = $response->name ?? '';
        $id = $response->id ?? null;
        $text = $response->text ?? '';
        return new Comment($name, $text, $id);
    }
}
