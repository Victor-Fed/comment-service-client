## Клиент для абстрактного (вымышленного) сервиса комментариев "example.com"

- Установка: ``` composer require victor-drom-test/comment-service-client ```
- Запуск тестов ``` .\vendor\bin\phpunit tests ```

- Пример использования :
```
<?php
require_once __DIR__ . '/vendor/autoload.php';

/* get comments */
$client = new Victor\clients\Example();
$result = $client->getComment();
var_dump($result);

/* Create comment */
$result = $client->addComment(new Victor\models\Comment('NameComment'));
var_dump($result);
```