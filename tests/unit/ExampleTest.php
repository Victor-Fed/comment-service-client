<?php
namespace Victor\tests\unit;

use Victor\clients\Example;
use Victor\http\RequestLibrary;
use Victor\models\Comment;

class ExampleTest extends \PHPUnit\Framework\TestCase
{
    public function testGetRequestSuccess()
    {
        $stub = $this->createMock(RequestLibrary::class);
        $stub->method('getResponse')
            ->willReturn(json_decode($this->getJsonList()));
        $stub->method('getHttpStatusCode')
            ->willReturn(200);
        $client = new Example($stub);
        $response = $client->getComment();
        $this->assertSame(200, $client->getHttpStatusCode());
        $this->assertSame('TestName1', $response[0]->name);
        $this->assertSame('TestName2', $response[1]->name);
    }

    public function testGetRequestFail()
    {
        $stub = $this->createMock(RequestLibrary::class);
        $stub->method('getResponse')
            ->willReturn(json_decode('{"status":"false"}'));
        $stub->method('getHttpStatusCode')
            ->willReturn(400);
        $client = new Example($stub);
        $client->getComment();
        $this->assertNotSame(200, $client->getHttpStatusCode());
    }

    public function testPostRequestSuccess()
    {
        $stub = $this->createMock(RequestLibrary::class);
        $stub->method('getResponse')
            ->willReturn(json_decode('{"id":1,"name":"TestName1","text":"Text"}'));
        $stub->method('getHttpStatusCode')
            ->willReturn(201);
        $client = new Example($stub);
        $response = $client->addComment(new Comment('Test', 'test'));
        $this->assertObjectHasAttribute('name', $response);
        $this->assertSame(201, $client->getHttpStatusCode());
    }

    public function testPostRequestFail()
    {
        $stub = $this->createMock(RequestLibrary::class);
        $stub->method('getResponse')
            ->willReturn(json_decode('{"status":"false"}'));
        $stub->method('getHttpStatusCode')
            ->willReturn(400);
        $client = new Example($stub);
        $client->addComment(new Comment('Test', 'test'));
        $this->assertNotSame(201, $client->getHttpStatusCode());
    }


    public function testPutRequestSuccess()
    {
        $stub = $this->createMock(RequestLibrary::class);
        $stub->method('getResponse')
            ->willReturn(json_decode('{"id":1,"name":"TestName1","text":"Text"}'));
        $stub->method('getHttpStatusCode')
            ->willReturn(200);
        $client = new Example($stub);
        $response = $client->updateComment(1, new Comment('tt'));
        $this->assertObjectHasAttribute('name', $response);
        $this->assertSame(200, $client->getHttpStatusCode());
    }

    public function testPutRequestFail()
    {
        $stub = $this->createMock(RequestLibrary::class);
        $stub->method('getResponse')
            ->willReturn(json_decode('{"status":"false"}'));
        $stub->method('getHttpStatusCode')
            ->willReturn(400);
        $client = new Example($stub);
        $response = $client->updateComment(1, new Comment('tt'));
        $this->assertNotSame(200, $client->getHttpStatusCode());
    }

    protected function getJsonList()
    {
        return '[{"id":1,"name":"TestName1","text":"Text"},{"id":2,"name":"TestName2","text":"Text"}]';
    }
}
